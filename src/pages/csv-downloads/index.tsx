import React, { useState } from 'react';
import SearchBar from 'src/components/search-bar';
import Country from 'src/components/country';

const countries = [
  'poland',
  'unitedstates',
  'germany',
  'unitedkingdom',
  'finland',
  'france'
];

const CsvDownloadsPage = (): JSX.Element => {
  const searchCallback = (search: string) => setState(search);
  const [search, setState] = useState('');
  const getCountries = () => {
    return countries.filter((c) => c.includes(search));
  };
  return (
    <>
      <SearchBar searchCallback={searchCallback} />
      {getCountries().map((c) => (
        <Country country={c} countryDisplay={c} />
      ))}
    </>
  );
};

export default CsvDownloadsPage;
